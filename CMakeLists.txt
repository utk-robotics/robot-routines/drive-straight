cmake_minimum_required(VERSION 3.1)
project(template_routine)

message("Building ${PROJECT_NAME}...")

add_subdirectory(scripts)

# Get all .cpp files
file(GLOB_RECURSE ${PROJECT_NAME}_SOURCES "src/*.cpp")
file(GLOB_RECURSE ${PROJECT_NAME}_HEADERS "include/**/*.hpp")

add_library(${PROJECT_NAME} ${${PROJECT_NAME}_SOURCES} ${${PROJECT_NAME}_HEADERS})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)
target_link_libraries(${PROJECT_NAME} rip fmt)
target_include_directories(${PROJECT_NAME} PUBLIC include)

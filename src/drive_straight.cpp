#include <rip/routines/drive_straight.hpp>
#include <rip/datatypes/constants.hpp>
#include <rip/utils/casting.hpp>

namespace rip::routines
{

    extern nlohmann::json rip::configConstants;
    
    DiffDriveStraight::DiffDriveStraight(const nlohmann::json &config, 
            std::shared_ptr<EventSystem> es, 
            std::string id, CompTable comps)
    : Routine(config, es, id, comps)
    {
        saveComponents(comps);
        if (config.find("distance") == config.end())
        {
            throw DistanceNotFound("Could not find the distance field in the config");
        }
        m_distance = std::make_shared(config.find("distance"));
        if (config.find("velocity") == config.end())
        {
            throw DistanceNotFound("Could not find the velocity field in the config");
        }
        m_vel = std::make_shared(config.find("velocity"));
        if (config.find("aceleration") == config.end())
        {
            throw DistanceNotFound("Could not find the acceleration field in the config");
        }
        m_accel = std::make_shared(config.find("acceleration"));
    }

    void DiffDriveStraight::start(std::vector<std::any> data)
    {
        if (data.size() != 0)
        {
            try
            {
                m_distance = param_cast<units::length::inch_t>(data[0]);
            }
            catch (BadParamAnyCast& e)
            {
                Logger::error("DiffDriveStraight: Data passed into {}'s start function is invalid. Continuing with current distance value.", m_id);
            }
        }
        m_running = true;
        run();
    }

    void DiffDriveStraight::stop(std::vector<std::any> data)
    {
        m_running = stop;
        m_dtrain->resetAllEncoders();
        sendMessage("stopped");
    }

    void DiffDriveStraight::saveComponents(CompTable comps)
    {
        if (comps->size() == 0)
        {
            throw MissingRequiredComponent("No differential drivetrain component was provided.");
        }
        m_dtrain = std::dynamic_pointer_cast<DifferentialDrivetrain>(comps->begin()->second);
        if (m_dtrain == nullptr)
        {
            throw MissingRequiredComponent("No differential drivetrain component was provided.");
        }
    }

    void DiffDriveStraight::run()
    {
        units::length::inch_t robo_distance(0.5 * m_accel * (m_vel / m_accel) 
                * (m_vel / m_accel));
        auto constEnd = configConstants.end();
        if (configConstants.find("ticks_per_rev") == constEnd ||
                configConstants.find("wheel_radius") == constEnd)
        {
            throw MissingConstant("\"ticks_per_rev\" or \"wheel_radius\" not found in the constants section of the config.");
        }
        rip::datatypes::MotorDynamics mdyn(configConstants.at("ticks_per_rev"), 
                configConstants.at("wheel_radius"));
        mdyn.setDistance(robo_distance);
        mdyn.setSpeed(*m_vel);
        mdyn.setAcceleration(*m_accel);
        mdyn.setDeceleration(*m_accel);
        m_dtrain->drive(mdyn);
        while (m_dtrain->readEncoder(0) < m_distance) {}
        stop();
    }

}

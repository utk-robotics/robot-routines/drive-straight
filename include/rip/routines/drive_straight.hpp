#ifndef DRIVE_STRAIGHT_DIFF_DRIVETRAIN_HPP
#define DRIVE_STRAIGHT_DIFF_DRIVETRAIN_HPP

#include <rip/routine.hpp>
#include <rip/dab/differential_drivetrain.hpp>

namespace rip::routines 
{

    NEW_RIP_EX(DistanceNotFound)
    NEW_RIP_EX(ThresholdNotFound)

    class DiffDriveStraight : public Routine
    {
        public:

            DiffDriveStraight(const nlohmann::json &config, std::shared_ptr<EventSystem> es, 
                    std::string id, CompTable comps);

            virtual void start(std::vector<std::any> data = {}) override;

            virtual void stop(std::vector<std::any> data = {}) override;

        protected:

            virtual void saveComponents(CompTable comps) override;

            virtual void run() override;

            virtual void handle_subscriptions(std::string handle, std::string func_id, std::string routine_id="") override {}

        private:

            std::shared_ptr<DifferentialDrivetrain> m_dtrain;

            std::shared_ptr<units::length::inch_t> m_distance;

            std::shared_ptr<units::velocity::inches_per_second> m_vel;

            std::shared_ptr<units::acceleration::inches_per_second_squared> m_accel;
    };

}

#endif // DRIVE_STRAIGHT_DIFF_DRIVETRAIN_HPP
